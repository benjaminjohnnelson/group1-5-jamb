from django.conf import settings
from django import forms
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseNotFound
from django_mako_plus import view_function
from django.contrib.auth import authenticate, login
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string
from account import models as amod
from catalog import models as cmod
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist

@staticmethod
def record_sale(user, shopping_cart, address, city, state, zipcode, stripe_token):

    sale = Sale()
    sale.user = user
    sale.address = address
    sale.city = city
    sale.state = state
    sale.zipcode = zipcode

    for item in shopping_cart:
        citem = cmod.SaleItem()
        citem.sale = sale
        citem.product = item.product
        citem.quantity = item.quantity
        citem.weight = item.weight
        citem.price = item.price
        citem.save()

    # Calculate subtotal
    for item in cmod.SIP.objects.filter(sale=sale):
        subtotal = subtotal + item.price


    shipping_cost = 4.99
    shippingSaleItem(
        sale=sale,
        shipping_cost=shipping_cost,
    )

    # TaxSaleItem
    tax_rate = 0.07
    tax_cost = subtotal * tax_rate
    taxSaleItem(
        sale=sale,
        rate=rate,
        tax_cost=tax_cost,
    )

    sale.total = subtotal + tax_cost + shipping_cost
    sale.save()

    # payment info
    payment = Payment()
    payment.stripe_token = stripe_token
    payment.sale = sale
    payment.save()

