from django.core import management
from django.db import connection
from datetime import datetime
import os, os.path, sys


# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')


# imports for our project
from account.models import FomoUser

# User object
# make 3
u1 = FomoUser()
u1.first_name = 'Paul'
u1.set_password('Password')
u1.save()