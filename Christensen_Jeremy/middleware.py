def Last5ProductsMiddleware(get_response):


    def middleware(request):
        print('>>>>>>beginning of middleware')

        request.last5 = request.session.get('last5products')
        if request.last5 is None:
            request.last5 = []

        # call view function here
        response = get_response(request)

        request.session['last5'] = request.last5[:5]

        print('>>>>>end of middleware')
        return response


    return middleware
