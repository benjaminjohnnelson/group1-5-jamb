from django.conf import settings
from django_mako_plus import view_function
from django.http import HttpResponseRedirect
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):

    # set variable for requested products id
    pid = request.urlparams[0]

    try:
        product = cmod.Product.objects.get(id=pid)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index')

    #create variable for last 5
    last5 = request.last5

    # add product to last5 as the first that appears
    last5.insert(0, product.id)
    # or last5.append(product.id) to add to end of list

    # if product already in last5, delete
    if product.id in last5:
        del last5[last5.index(product.id)]

    return dmp_render(request, 'detail.html', {
        'product': product,

    })