from django.db import models

# Create your models here.

class FomoUser(AbstractUser):

    address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zipcode = models.TextField(null=True, blank=True)
    birthdate = models.TextField(null=True, blank=True)
    pref_contact = models.TextField(null=True, blank=True, choices = CONTACT_CHOICES)