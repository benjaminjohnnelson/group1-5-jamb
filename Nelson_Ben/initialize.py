##Initialize this crap!
import django
import os
from datetime import datetime
from account.models import FomoUser
# intialize the django env
##the envirnment is where this is. Fomo1
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo1.settings'
django.setup()


##The user for all of this
# make 3 users
# u1 = FomoUser()
# u1.set_password('password1')
# u1.last_login = datetime(2017, 1, 25, 5, 0)
# u1.username = 'User1'
# u1.first_name = 'User'
# u1.last_name = '1'
# u1.email = 'user1@gmail.com'
# u1.is_staff = "True"
# u1.us_active = "True"
# u1.date_joined = datetime(2016, 1, 25, 5, 0)
# u1.street_address = 'Michigan'
# u1.city = 'Nampa'
# u1.state = 'ID'
# u1.zip_code = '83687'
# u1.phone = '111-111-1111'
# u1.birthday = datetime(1992, 9, 21, 5, 0)
# u1.save()

# u2 = FomoUser()
# u2.set_password("password1")
# u2.last_login = datetime(2017, 1, 25, 5, 0)
# u2.username = 'User2'
# u2.first_name = 'User'
# u2.last_name = '2'
# u2.email = 'user2@gmail.com'
# u2.is_staff = "True"
# u2.us_active = "True"
# u2.date_joined = datetime(2016, 1, 27, 5, 0)
# u2.street_address = 'Idaho'
# u2.city = 'Nampa'
# u2.state = 'ID'
# u2.zip_code = '83687'
# u2.phone = '222-222-2222'
# u2.birthday = datetime(1992, 9, 22, 5,0)
# u2.save()
#
# u3 = FomoUser()
# u3.set_password("password1")
# u3.last_login = datetime(2017, 1, 25, 5, 0)
# u3.username = 'User3'
# u3.first_name = 'User'
# u3.last_name = '3'
# u3.email = 'user3@gmail.com'
# u3.is_staff = "True"
# u3.us_active = "True"
# u3.date_joined = datetime(2016, 1, 23, 5, 0)
# u3.street_address = 'Idaho'
# u3.city = 'Nampa'
# u3.state = 'ID'
# u3.zip_code = '83687'
# u3.phone = '333-333-3333'
# u3.birthday = datetime(1992, 9, 23, 5, 0)
# u3.save()
#
# u4 = FomoUser()
# u4.set_password("password1")
# u4.last_login = datetime(2017, 1, 25, 5, 0)
# u4.username = 'bennelson'
# u4.first_name = 'Ben'
# u4.last_name = 'Nelson'
# u4.email = 'user3@gmail.com'
# u4.is_staff = "True"
# u4.us_active = "True"
# u4.date_joined = datetime(2016, 1, 23, 5, 0)
# u4.street_address = 'Fenway'
# u4.city = 'Boston'
# u4.state = 'MA'
# u4.zip_code = '83687'
# u4.phone = '444-444-4444'
# u4.birthday = datetime(1992, 9, 23, 5, 0)
# u4.save()

# query all with different query options (3-5 examples)
# #this returns the user 2
user = FomoUser.objects.filter(first_name__contains='User')
for u1 in user:
    print(u1.first_name, u1.last_name, u1.phone)
# #everything that is less than 5
users = FomoUser.objects.filter(id__lt=5)
for u1 in users:
    print(u1.last_name)

##this returns all the good stuff if you know what I mean
##everything that is not less than 2
user = FomoUser.objects.exclude(id__lt=2)
for u1 in user:
    print(u1.first_name, u1.last_name, u1.phone)

# #everything that is greater than 100
# users = FomoUser.objects.exclude(id__gt=100)
# #Zip_code that is the same as 83687
user = FomoUser.objects.filter(zip_code__contains='83687')
for u1 in user:
    print(u1.first_name, u1.last_name, u1.phone, u1.zip_code)



#
# # creates a new object (row) in the database
# q = Question()
# q.question_text = "Is it snowing today?"
# q.pub_date = datetime(2017, 6, 1, 5, 0)
# q.save()
#
# c1 = Choice()
# c1.question = q
# c1.choice.text = 'Yes'
# c1.save()
#
#
# # grabs it from the database based on id
# q1 = Question.objects.get(id=3)
#
# # grabs it from the database based on data value
# q2 = Question.objects.filter(question_text='Is it snowing today?')
# for q1 in questions:
#     print(q1.question_text)
#
# # grabs all of the question objects
# questions = Question.objects.all()
#
#
# # deletes a question
# q1.delete()
#
# # grabs those with id less than 5
# questions = Question.objects.filter(id__lt=5)
#
# # opposite of above, a not statement
# questions = Question.objects.exclude(id__lt=5)
#
# # get those not under 5, and contains the word today
# questions = Question.objects.exclude(id__lt=5).filter(question_text__contains='today')
#
#
#
#
#
# # to run this file from cmd
# # python initialize.py
