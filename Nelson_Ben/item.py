from django.conf import settings
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from .. import dmp_render, dmp_render_to_string
from catalog import models as cmod


@view_function
## View function only is used so that it can be called from a URL
def process_request(request):
    ##def - defintion (defines function)
    ##process_request --- is the default function.
    ## request object_ holds all the information for that request such as the USER, URLPARAMS attached by the middleware
    print(">>> Entering the process_request function ...")
    
    # product = get_object_or_404(cmod.Product, pk=request.urlparams[0])
    try:
        lastf = request.last5
        lastfDisplay = []
        for p in lastf:
            ##debugging print
            print(p)
            tempProduct = cmod.Product.objects.get(id=p)
            ##debugging print
            print(tempProduct.name)
            ##apending the temp products.
            lastfDisplay.append(tempProduct)
            lastfDisplay = list(reversed(lastfDisplay))
        ##Loading the object.
        product = cmod.Product.objects.get(id=request.urlparams[0])
        ##this is loading the last five stuff.
        lastfive = request.last5
        print(">>>>>>>>>>>>>>>>> last5", lastfive)
        lastfive.insert(0, product.id)
        ##if the length is over 6.
        if len(lastfive) == 6:
            lastfive.pop()
        print(">>>>>>>>>>>>>>>>> add product to l5", lastfive)
        request.last5 = list(lastfive)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/items')


    print(">>> The product name is %s" % product.name)

context = {
    'product': product,
        'lastfive': lastfDisplay,
    }
    return dmp_render(request, 'item.html', context)
