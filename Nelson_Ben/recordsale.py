########################################################################################
###############################    CODE_REVIEW     #####################################
###############################                    #####################################
########################################################################################
@classmethod
def record_sale(cls, user, shopping_cart_items, stripe_token, street, city, state, zipcode):
   #create sale object
   sale = cls()
   sale.user = user
   sale.street = street
   sale.city = city
   sale.state = state
   sale.zipcode = zipcode
   sale.subtotal = sale.calcSubtotal()
   sale.tax = sale.calcTax()
   sale.shipping = sale.calcShipping()
   sale.total = sale.calcTotal()
   sale.save()
   ########################################################################################
   ########################################################################################
   #create Sale Item object(s)
   for i in cart:
       si = SaleItem()
       si.sale = sale
       si.product = i.product
       si.quantity = i.quantity
       si.price = i.price
       si.save()

   ########################################################################################
   ########################################################################################
   #create tax sale item object
   tax_amount = SaleItem()
   tax_amount.sale = sale
   tax_amount.product = null
   tax_amount.quantity = 1
   tax_amount.price = new_sale.tax
   tax_amount.save()

   ########################################################################################
   ##### The sales tax for this is going to be 7.25 percent
   ########################################################################################
   #create shipping sale item object
   shipping_amount = cmod.SaleItem()
   shipping_amount = new_sale
   shipping_amount.prodcut = null
   shipping_amount.quantity = 1
   shipping_amount.price = sale.shipping
   shipping_amount.save()

   ########################################################################################
   ########################################################################################
   payment = Payment()
   payment.sale = sale
   payment.SaleID
   payment.date = date.datetime(now)
   payment.stripe_token = stripe_token
   payment.amount = payment.calcTotal()
   payment.save()
