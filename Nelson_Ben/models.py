from django.db import models
from datetime import datetime
from django.contrib.auth.models import AbstractUser
#############
# account app
#############
# # What the data field shows up as to the user, like the displayname in c#
# Contact_Choices = [
#     [ 'text, 'Text' ],
#     [ 'email, 'Email' ],
#     [ 'voice, 'Voice' ],
# ]
# django.contrib.auth.base_user (for AbstractBaseUser)
# django.contrib.auth.models (for AbstractUser) -> inherits from above
# FomoUser -> inhertis from above

STATE_CHOICES = (
('AL', 'Alabama'),
('AZ', 'Arizona'),
('AR', 'Arkansas'),
('CA', 'California'),
('CO', 'Colorado'),
('CT', 'Connecticut'),
('DE', 'Delaware'),
('DC', 'District of Columbia'),
('FL', 'Florida'),
('GA', 'Georgia'),
('ID', 'Idaho'),
('IL', 'Illinois'),
('IN', 'Indiana'),
('IA', 'Iowa'),
('KS', 'Kansas'),
('KY', 'Kentucky'),
('LA', 'Louisiana'),
('ME', 'Maine'),
('MD', 'Maryland'),
('MA', 'Massachusetts'),
('MI', 'Michigan'),
('MN', 'Minnesota'),
('MS', 'Mississippi'),
('MO', 'Missouri'),
('MT', 'Montana'),
('NE', 'Nebraska'),
('NV', 'Nevada'),
('NH', 'New Hampshire'),
('NJ', 'New Jersey'),
('NM', 'New Mexico'),
('NY', 'New York'),
('NC', 'North Carolina'),
('ND', 'North Dakota'),
('OH', 'Ohio'),
('OK', 'Oklahoma'),
('OR', 'Oregon'),
('PA', 'Pennsylvania'),
('RI', 'Rhode Island'),
('SC', 'South Carolina'),
('SD', 'South Dakota'),
('TN', 'Tennessee'),
('TX', 'Texas'),
('UT', 'Utah'),
('VT', 'Vermont'),
('VA', 'Virginia'),
('WA', 'Washington'),
('WV', 'West Virginia'),
('WI', 'Wisconsin'),
('WY', 'Wyoming'))

class FomoUser(AbstractUser):

    ##Spacing for readability
    ##
    street_address = models.TextField(max_length=255)
    city = models.TextField(max_length=50)
    state = models.TextField(max_length=2, choices=STATE_CHOICES)
    zip_code = models.TextField(max_length=15)
    phone = models.TextField(max_length=20)
    birthday = models.DateField()
    # pref_contact = models.TextField(null=True, blank=True, choices=Contact_Choices)
    # inheritting from super
    # id
    # first_name
    # last_name
    # email, and more
    ##THis is teh birthday function we made.
def get_age(self):
    today = datetime.date.today()
    age_in_days = today - datetime.date(self.birthday)
    return age_in_days
# getting and setting data
# for tomorrow (code_review)     groups come up with DCD, just 1 class. 3 things on there (abstractbaseuser),
# complete the FomoUser
# create the intiialize file to match FomoUser, add a couple users
# push them to the new repo (just the two files - models.py, initialize.py, not whole project )
