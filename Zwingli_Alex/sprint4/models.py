from django.db import models
from datetime import datetime
from account import models as amod
from polymorphic.models import PolymorphicModel

# Create your models here.

class Category(models.Model):
    # id
    codename = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class Product(PolymorphicModel):
    '''
    - id (auto)
    - create_date (auto)
    - modified_date (auto)
    - price
    - name
    - brand
    - category (object)
    '''
    # id
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True) # 999,999.99
    name = models.TextField(blank=True, null=True)
    brand = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category')

class ProductPicture(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE,)
    subdir = models.TextField(blank=True, null=True)
    alttext = models.TextField(blank=True, null=True)
    # mimetype = models.force_text()
        # image/jpg, image/png, image/gif



class BulkProduct(Product):
    # id
    quantity = models.IntegerField(default=0)
    reorder_trigger = models.IntegerField(default=0)
    reorder_quantity = models.IntegerField(default=0)
    # vendor

class UniqueProduct(Product):
    # id
    serial_number = models.TextField(blank=True, null=True)
    sold = models.BooleanField(default=True)
    # vendor info

class RentalProduct(UniqueProduct):
    # id
    rental_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    rental_start = models.DateTimeField(blank=True, null=True)
    rental_end = models.DateTimeField(blank=True, null=True)

class CartItem(models.Model):
    '''
    Stores user, product, quantity, and date information to support active cart
    and cart history. Only stores most recent modification.
    - user (FK)
    - product (FK)
    - quantity
    - added_date (auto)
    - purchased_date
    - modified_date (auto)
    '''
    user = models.ForeignKey("account.FomoUser")
    product = models.ForeignKey("catalog.Product")
    quantity = models.IntegerField(blank=False, null=True)
    added_date = models.DateTimeField(auto_now_add=True)
    purchased_date = models.DateTimeField(blank=False, null=True)
    modified_date = models.DateTimeField(auto_now=True)

class Sale(models.Model):
    '''
    Stores individual sale information
    - id (auto)
    - user (FK)
    - date (auto)
    - shipping_address (FK)
    - total_ammount
    '''
    # id
    user = models.ForeignKey("account.FomoUser")
    date = models.DateTimeField(auto_now_add=True)
    shipping_address = models.ForeignKey("account.ShippingAddress")
    total_ammount = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=False)

class SaleItem(PolymorphicModel):
    '''
    Stores sale item and quantity for Sale
    - id (auto)
    - sale (FK)
    - product (FK)
    - quantity
    - sale_price
    '''
    # id
    sale = models.ForeignKey(Sale)

class TaxSaleItem(SaleItem):
    '''
    Extends Sale Item (id, sale) and stores sold product information
    - rate (decimal)
    - tax_ammount (decimal)
    '''
    rate = models.DecimalField(max_digits=1, decimal_places=2, blank=False, null=True)
    ammount = models.DecimalField(max_digits=4, decimal_places=2, blank=False, null=True)

class ShippingSaleItem(SaleItem):
    '''
    Extends Sale Item (id, sale) and stores shipping information
    - shipping_cost (decimal)
    '''
    pass

class ProductSaleItem(SaleItem):
    '''
    Extends Sale Item (id, sale) and stores sold product information
    - product (FK)
    - quantity (int)
    - sale_price (decimal)
    '''
    product = models.ForeignKey(Product)
    quantity = models.IntegerField(blank=False, null=True)
    sale_price = models.DecimalField(max_digits=8, decimal_places=2, blank=False, null=True)

class ViewHistoryItem(models.Model):
    '''
    Stores user, product, and date information to support recent history
    and analytics per user. Supports duplicate(+) views.
    - user
    - product
    - view_date (auto)
    '''
    user = models.ForeignKey("account.FomoUser")
    product = models.ForeignKey(Product)
    view_date = models.DateTimeField(auto_now=True)

def record_sale(request, shipping_address, paid):
    '''
    PARAMS
    - request (default)
    - shipping_address (object)
    - paid (decimal): ammount charged to user

    ACTIONS
    - create sale object
    - create SaleItem object(s)
    - update inventory and product availability
    - update shopping cart
    - create payment objects
    '''
    # Load user's current
    cart = request.user.shopping_cart()

    ## Create Sale (1/2)
    # Create sale header information
    sale = Sale()
    sale.user = request.user
    sale.shipping_address = shipping_address
    sale.save()

    ## Create SaleItem object(s)
    # ProductSaleItem
    for item in cart:
        entry = ProductSaleItem()
        entry.sale = sale
        entry.product = item.product
        entry.quantity = item.quantity
        entry.sale_price = item.product.price
        entry.save()

    # Sale subtotal
    subtotal = 0
    for item in ProductSaleItem.objects.filter(sale=sale):
        subtotal = subtotal + item.sale_price

    # ShippingSaleItem
    shipping = 9.99
    ShippingSaleItem(
        sale=sale,
        shipping_cost=shipping,
        )

    # TaxSaleItem
    rate = 0.08
    tax_ammount = subtotal * rate
    TaxSaleItem(
        sale=sale,
        rate=rate,
        tax_ammount=tax_ammount,
    )

    ## Create Sale (2/2)
    sale.total_ammount = shipping + subtotal + tax_ammount
    sale.save()

    ## Update inventory and product availability
    for item in cart:
        # load product
        product = Product.objects.get(id=item.id)
        # unique -> mark as sold
        if hasattr(item, 'sold'):
            product.sold = False
        # bulk -> update quantity
        else:
            product.quantity = product.quantity - item.quantity
        # Update cart
        item.purchased_date = datetime.now()
        item.save()

    ## Create payment objects
    payment = amod.Payment()
    payment.sale = sale
    payment.payment_ammount = paid
    payment.save()
