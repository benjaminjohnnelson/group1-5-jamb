def LastFiveMiddleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        #taking in the request
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        print('>>>>>>>>> middleware call')

        # pull last 5 list out of the session
        last5_list = request.session.get('last5')
        if last5_list is None:
            last5_list = []
        request.last5 = last5_list

        # return to django
        response = get_response(request)

        #only getting the last 5 items
        request.session['last5'] = request.last5[:5]

        # Code to be executed for each request/response after
        # the view is called.
        print('>>>>>>>>> middleware called at end')
        request.session['last5'] = request.last5
        return response

    #returning this specific middleware
    return middleware
