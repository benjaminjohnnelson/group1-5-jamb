''' Logic behind product details view '''

from datetime import datetime
from django.shortcuts import redirect
from django_mako_plus import view_function
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    ''' Fetches and displays instrument details based on request from catalog/index page '''

    # save instrument selection - provided through link
    selection = request.urlparams[0]
    print('>>>', selection)

    # load appropriate product object
    try: # unique product
        product = cmod.UniqueProduct.objects.get(id=selection)
        product_type = 'unique'
        print('>>>', product)
    except cmod.UniqueProduct.DoesNotExist:
        try: # bulk product
            product = cmod.BulkProduct.objects.get(id=selection)
            product_type = 'bulk'
            print('>>>', product)
        except cmod.BulkProduct.DoesNotExist:
            try: # rental product
                product = cmod.RentalProduct.objects.get(id=selection)
                product_type = 'rental'
                print('>>>', product)
            except cmod.RentalProduct.DoesNotExist:
                # redirect to all listings if product not fount
                return redirect('/catalog/index')

    # load last 5 searched products
    last5 = request.last5

    # configure history to pass to view_function
    history = list(map(lambda item: cmod.Product.objects.get(id-item), last5))


    # update recent product history
    if product.id in last5: 
        # delete duplicate
        del last5[last5.index(product.id)]
    else: 
        # add to history
        last5.insert(0, product.id)

    # load product image
    try:
        product_images = cmod.ProductPicture.objects.all().filter(product_id=selection)
        print('>>>', 'Loaded Pictures')
    except cmod.ProductPicture.DoesNotExist:
        product_images = None


    return dmp_render(request, 'details.html', {
        'now': datetime.now(),
        'product': product,
        'type': product_type,
        'image': product_images.first(),
        'images': product_images,
        'history': history,
    })
