import os
from django.db import connection
from django.core.management import call_command
from datetime import datetime

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()

# drop and recreate the database tables
print()
print('Living on the edge! Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute('DROP SCHEMA public CASCADE')
    cursor.execute('CREATE SCHEMA public')
    cursor.execute('GRANT ALL ON SCHEMA public TO postgres')
    cursor.execute('GRANT ALL ON SCHEMA public TO public')

# make the migrations and migrate
call_command('makemigrations')
call_command('migrate')

# imports from our project
from account.models import FomoUser

# FomoUser object

# Populate Users (5)

## User 1 ##
u = FomoUser()
u.first_name = 'Jason'
u.last_name = 'Castello'
u.username = 'jcastello'
u.birthdate = datetime(1992, 1, 15)
u.email = 'jason.castello@gmail.com'
u.phone = '787-247-9291'
u.pref_contact = 'email'
u.address_1 = '3101 Jackson St'
u.address_2 = 'Apt. 24'
u.city = 'Oxnard'
u.state = 'CA'
u.zipcode = '93033'
u.save()

## User 2 ##
u = FomoUser()
u.first_name = 'Martha'
u.last_name = 'James'
u.username = 'mjames'
u.birthdate = datetime(1987, 8, 4)
u.email = 'martha.james@gmail.com'
u.phone = '610-429-4567'
u.pref_contact = 'text'
u.address_1 = '110 E Kelly Ave'
u.address_2 = ''
u.city = 'Jackson'
u.state = 'WY'
u.zipcode = '83001'
u.save()


## User 3 ##
u = FomoUser()
u.first_name = 'Skye'
u.last_name = 'Jensen'
u.username = 'sjensen'
u.birthdate = datetime(1990, 7, 6)
u.email = 'jason.castello@gmail.com'
u.phone = '702-879-8464'
u.pref_contact = 'voice'
u.address_1 = '101 May Cove'
u.address_2 = 'Unit 19'
u.city = 'Georgetown'
u.state = 'TX'
u.zipcode = '78626'
u.save()


## User 4 ##
u = FomoUser()
u.first_name = 'Chris'
u.last_name = 'Bacon'
u.username = 'cbacon'
u.birthdate = datetime(1995, 12, 25)
u.email = 'chris.p.bacon@gmail.com'
u.phone = '850-934-1225'
u.pref_contact = 'text'
u.address_1 = '2634 Highland Park Dr'
u.address_2 = ''
u.city = 'Pigeon Forge'
u.state = 'TN'
u.zipcode = '37863'
u.save()


## User 5 ##
u = FomoUser()
u.first_name = 'Darcy'
u.last_name = 'Johnson'
u.username = 'djohnson'
u.birthdate = datetime(2000, 2, 29)
u.email = 'darcy.johnson@gmail.com'
u.phone = '616-472-3925'
u.pref_contact = 'email'
u.address_1 = '311 Leafland Ave'
u.address_2 = ''
u.city = 'Nashville'
u.state = 'TN'
u.zipcode = '37210'
u.save()
