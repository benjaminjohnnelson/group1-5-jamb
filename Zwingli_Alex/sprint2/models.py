from datetime import date
from django.db import models
from django.contrib.auth.models import AbstractUser


CONTACT_CHOICES = [
    ['text', 'Text'],
    ['email', 'Email'],
    ['voice', 'Voice'],
]

STATE_CHOICES = [
    ['AL', 'Alabama'],
    ['AZ', 'Arizona'],
    ['AR', 'Arkansas'],
    ['CA', 'California'],
    ['CO', 'Colorado'],
    ['CT', 'Connecticut'],
    ['DE', 'Delaware'],
    ['DC', 'District of Columbia'],
    ['FL', 'Florida'],
    ['GA', 'Georgia'],
    ['ID', 'Idaho'],
    ['IL', 'Illinois'],
    ['IN', 'Indiana'],
    ['IA', 'Iowa'],
    ['KS', 'Kansas'],
    ['KY', 'Kentucky'],
    ['LA', 'Louisiana'],
    ['ME', 'Maine'],
    ['MD', 'Maryland'],
    ['MA', 'Massachusetts'],
    ['MI', 'Michigan'],
    ['MN', 'Minnesota'],
    ['MS', 'Mississippi'],
    ['MO', 'Missouri'],
    ['MT', 'Montana'],
    ['NE', 'Nebraska'],
    ['NV', 'Nevada'],
    ['NH', 'New Hampshire'],
    ['NJ', 'New Jersey'],
    ['NM', 'New Mexico'],
    ['NY', 'New York'],
    ['NC', 'North Carolina'],
    ['ND', 'North Dakota'],
    ['OH', 'Ohio'],
    ['OK', 'Oklahoma'],
    ['OR', 'Oregon'],
    ['PA', 'Pennsylvania'],
    ['RI', 'Rhode Island'],
    ['SC', 'South Carolina'],
    ['SD', 'South Dakota'],
    ['TN', 'Tennessee'],
    ['TX', 'Texas'],
    ['UT', 'Utah'],
    ['VT', 'Vermont'],
    ['VA', 'Virginia'],
    ['WA', 'Washington'],
    ['WV', 'West Virginia'],
    ['WI', 'Wisconsin'],
    ['WY', 'Wyoming']
]

# Define models here

class FomoUser(AbstractUser):
    """Extended model for basic user."""
    #ineriting from super
    # id
    # first_name
    # last_name
    # emai

    address_1 = models.TextField(max_length=30, null=True, blank=False)
    address_2 = models.TextField(max_length=30, null=True, blank=False)
    city = models.TextField(null=True, blank=True)
    state = models.CharField(max_length=2, null=True, blank=False, choices=STATE_CHOICES)
    zipcode = models.TextField(max_length=30, null=True, blank=False)
    birthdate = models.DateField(null=True, blank=True)
    phone = models.TextField(max_length=30, null=True, blank=False)
    pref_contact = models.TextField(null=True, blank=True, choices=CONTACT_CHOICES)

    def get_age(self):
        """Returns current age of user."""
        today = date.today()
        print(today)
        if (today.month, today.day) < (self.birthdate.month, self.birthdate.day):
            print('Upcoming birthday')
            return today.year - self.birthdate.year - 1
        else:
            print('birthday passed')
            return today.year - self.birthdate.year
