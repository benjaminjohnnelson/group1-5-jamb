############
# Account #
############

#importing models
from django.db import models
from datetime import date
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _

'''CONTACT_CHOICES = [
    ['text', 'Text'],
    ['email', 'Email'],
    ['voice', 'Voice'],
]'''

class FomoUser(AbstractUser):
    #creating FomoUser Class for all users. Inherits from AbstractUser
    #############################
    #inheriting the following items from AbstractUser
    #username
    #first_name
    #last_name
    #email
    #is_staff
    #is_active
    #date_joined

    #adding additional attributes
    address_1 = models.TextField(_('address 1'), max_length=30, null=True, blank=True)
    address_2 = models.TextField(_('address 2'), max_length=30,null=True, blank=True)
    city = models.TextField(_('city'), max_length=30,null=True, blank=True)
    state = models.CharField(_('state'),max_length=2,null=True, blank=True)
    zipcode = models.IntegerField(null=True, blank=True)
    perferred_name = models.CharField(_('perferred name'), max_length=30, blank=True)
    phone = models.IntegerField(default='0000',null=True, blank=True)
    secondary_contact = models.TextField(max_length=100, null=True)
    birthdate = models.DateField(null=True, blank=True)

    def get_age(self):
        #returns age of the individual
        today = date.today()
        return today.year - birthdate.year - ((today.month, today.day) < (birthdate.month, birthdate.day))
