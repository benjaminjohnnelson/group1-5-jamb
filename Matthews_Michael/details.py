from django.conf import settings
from django_mako_plus import view_function
from .. import dmp_render, dmp_render_to_string
from catalog import models as cmod
from django.http import HttpResponse, HttpResponseRedirect


@view_function
def process_request(request):
    '''
    Seeing a specific product item and its details
    '''

    try:
        #seeing if we can get the specific id that was passed to us
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index')

    #instaintaining a local variable of the last 5 items
    last5 = request.last5

    #blackmagic
    #this right here will talk the item and add it to the history
    #so that I can access it
    history = list(map(lambda item: cmod.Product.objects.get(id=item), last5))

    if product.id in last5:
        #deleting any duplicate products in the last5
        del last5[last5.index(product.id)]

    #adding item into the last5 if it isn't already there
    #setting it to the first item in the list (i.e. 0)
    last5.insert(0,product.id)

    return dmp_render(request, 'details.html', context = {
           'product': product,
           'history': history,
           })
