"""
I decided not to create a separate sale item for taxes or subtotal
"""
@classmethod
def record(cls, user, shopping_cart_items, stripe_token, street, city, state, zipcode):
    #creating a new sale item
    sale = cls()
    sale.user = user
    sale.street = street
    sale.city = city
    sale.state = state
    sale.zipcode = zipcode

    for i in shopping_cart_items:
        #creating sale items out of shopping cart items
        #all of the shopping cart items are being created for SaleItems
        si = SaleItem()
        si.sale = sale #tying each sale_itme to the sale
        si.qty = i.quantity
        si.price = i.price
        si.product = i.product
        si.save()
    #returning the information needed to record the sale
    #can be used by the view if necessary

    #creating a new payment information
    payment = Payment()
    payment.stripe_token = stripe_token
    payment.sale = sale
    payment.save()

    #methods to calculate other information on the sale
    sale.tax = sale.calcTax()
    sale.subtotal = sale.calcSubTotal()
    sale.shipping_cost = sale.calcShippingCost()
    sale.total = sale.calcTotal()
    sale.save()

    #this isn't necessarily necessary
    return sale
