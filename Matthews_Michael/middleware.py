def LastFiveMiddleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        #taking in the request
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        #printing for debugging purposes
        print('>>>>>>>>> middleware call>>>>>>>>>>>>')

        #creating the last5 list
        last5_list = request.session.get('last5')

        if last5_list is None:
            #setting as a list if it is nothign yet
            last5_list = []

        request.last5 = last5_list

        response = get_response(request)

        #only getting the last 5 items
        request.session['last5'] = request.last5[:5]

        # Code to be executed for each request/response after
        # the view is called.

        return response

    #returning this specific middleware
    return middleware
