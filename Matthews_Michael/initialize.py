import os
import django
from django.db import connection
from django.core.management import call_command
# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'

#django setup
django.setup()

#creating blank space for readability
print()
print('Dropping Schema')
print()

#dropping all tables
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

#making migartions
call_command('makemigrations')
call_command('migrate')

#calling in the account models the FomoUser class
from account.models import FomoUser

#intiliazing db

#Creation of three users

### USER 1 ####
user1 = FomoUser()
user1.first_name = 'Kelly'
user1.last_name = 'Matthews'
user1.perferred_name = 'Kel'
user1.email = 'kelly@test.com'
user1.username = 'kel23469622'
user1.address_1 = '423 White Road'
user1.zipcode = 2
user1.state = 'UT'
user1.city = 'Provo'
user1.save()

### USER 2 ####
user2 = FomoUser()
user2.first_name = 'Jake'
user2.last_name = 'Browning'
user2.perferred_name = 'Jacob'
user2.email = 'jay@test.com'
user2.username = 'jay246962'
user2.address_1 = '423 White Road'
user2.zipcode = 2
user2.state = 'UT'
user2.city = 'Provo'
user2.save()

### USER 3 ####
user3 = FomoUser()
user3.first_name = 'Scott'
user3.last_name = 'Collins'
user3.perferred_name = 'Scotty'
user3.email = 'scot@test.com'
user3.username = 'scot26462392'
user3.address_1 = '999 White Road'
user3.state = 'UT'
user3.city = 'Provo'
user3.save()

print("\n\n")
print("Preparing to print test queiries")
print("\n\n")

#defining queries
allUsers = FomoUser.objects.all()
getOne = FomoUser.objects.get(id=1)
getRoad = FomoUser.objects.filter(address_1='423 White Road')

#possible outputs to loop through
outputs = [
    [allUsers, "Getting all the users in the db \n"],
    [getOne, "Getting just one user \n"],
    [getRoad, "Filtering based upon the road they live on \n"]
]

for x in outputs:
    #outputting all of the different variables
    print(x[1])
    print(x[0])

print("\n Done \n")
